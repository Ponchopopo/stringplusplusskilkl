
#include <iostream>
#include <string>
using namespace std;
int main()
{
    cout << "Enter your number: ";
    string number;
    getline(cin, number);

    cout << number << " " << "\n";
    cout << "Length of your string is " << number.length() << endl;
    cout << "The first symbol " << number[0] << endl;
    cout << "The last symbol " << number[number.length() - 1] << endl;
}